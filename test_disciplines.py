# import hashlib
# import json
import os

from jsonschema import validate

from api_tester import (get_response, load_schema,
                        NO_ACCEPT_SCHEMA, NO_USERAGENT_SCHEMA,
                        WRONG_ACCEPT_SCHEMA, WRONG_USERAGENT_SCHEMA,
                        WRONG_TOKEN_SCHEMA, DELETE_NOT_ALLOWED,
                        PATCH_NOT_ALLOWED, POST_NOT_ALLOWED,
                        PUT_NOT_ALLOWED)
from test_student import STUDENT_SCHEMA_PATH

URI = 'https://orioks.miet.ru/api/v1/student/disciplines'

DISCIPLINES_SCHEMA_PATH = STUDENT_SCHEMA_PATH + 'disciplines/'
DISCIPLINES_200_SCHEMA = DISCIPLINES_SCHEMA_PATH + '200.json'


def test_disciplines_without_accept_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Accept': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_ACCEPT_SCHEMA)) is None


def test_disciplines_with_wrong_accept_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Accept': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_ACCEPT_SCHEMA)) is None


def test_disciplines_without_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'User-Agent': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_USERAGENT_SCHEMA)) is None


def test_disciplines_with_wrong_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'User-Agent': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_USERAGENT_SCHEMA)) is None


def test_disciplines_without_authorization_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Authorization': None})
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_disciplines_with_wrong_credentials():
    status_code, body_json = get_response(
        'GET', URI, send_wrong_credentials=True)
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_disciplines_with_wrong_delete_method():
    status_code, body_json = get_response('DELETE', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(DELETE_NOT_ALLOWED)) is None


def test_disciplines_with_wrong_post_method():
    status_code, body_json = get_response('POST', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(POST_NOT_ALLOWED)) is None


def test_disciplines_with_wrong_put_method():
    status_code, body_json = get_response('PUT', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(PUT_NOT_ALLOWED)) is None


def test_disciplines_with_wrong_patch_method():
    status_code, body_json = get_response('PATCH', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(PATCH_NOT_ALLOWED)) is None


def test_disciplines_with_right_credentials():
    status_code, body_json = get_response('GET', URI)
    assert status_code == 200
    assert validate(body_json, load_schema(DISCIPLINES_200_SCHEMA)) is None
    os.environ['DISCIPLINE_ID'] = str(body_json[0]['id'])
#    encoded_body_json = json.dumps(body_json).encode()
#    global current_disciplines_hash
#    current_disciplines_hash = hashlib.sha512(encoded_body_json).hexdigest()


# def test_disciplines_with_right_credentials_and_next_semester():
#     status_code, body_json = get_response(
#             'GET', URI, data={'year': '2019-2020', 'semester': 2})
#     assert status_code == 200
#     assert validate(body_json, load_schema(DISCIPLINES_200_SCHEMA)) is None
#     encoded_body_json = json.dumps(body_json).encode()
#     next_disciplines_hash = hashlib.sha512(encoded_body_json).hexdigest()
#     assert current_disciplines_hash != next_disciplines_hash
