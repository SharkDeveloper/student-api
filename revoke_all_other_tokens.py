#!/usr/bin/env python
import os

from api_tester import get_response
from test_tokens import URI

assert os.environ.get('ORIOKS_TOKEN') is not None
status_code, body_json = get_response('GET', URI)
assert status_code == 200
tokens = [obj['token'] for obj in body_json[:-1]]  # все, кроме последнего
for t in tokens:
    uri_to_revoke = '{}/{}'.format(URI, t)
    get_response('DELETE', uri_to_revoke)
