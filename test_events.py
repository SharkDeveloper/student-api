import os

from jsonschema import validate

from api_tester import (get_response, load_schema,
                        NO_ACCEPT_SCHEMA, NO_USERAGENT_SCHEMA,
                        WRONG_ACCEPT_SCHEMA, WRONG_USERAGENT_SCHEMA,
                        WRONG_TOKEN_SCHEMA, DELETE_NOT_ALLOWED,
                        PATCH_NOT_ALLOWED, POST_NOT_ALLOWED,
                        PUT_NOT_ALLOWED)
from test_disciplines import DISCIPLINES_SCHEMA_PATH


EVENTS_SCHEMA_PATH = DISCIPLINES_SCHEMA_PATH + 'events/'
EVENTS_200_SCHEMA = EVENTS_SCHEMA_PATH + '200.json'
EVENTS_400_SCHEMA = EVENTS_SCHEMA_PATH + '400.json'


def setup_module():
    discipline_id = os.environ.get('DISCIPLINE_ID')
    assert discipline_id is not None
    global URI
    URI = 'https://orioks.miet.ru/api/v1/student/disciplines/{}/events'.format(
        discipline_id)


def test_events_without_accept_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Accept': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_ACCEPT_SCHEMA)) is None


def test_events_with_wrong_accept_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Accept': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_ACCEPT_SCHEMA)) is None


def test_events_without_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'User-Agent': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_USERAGENT_SCHEMA)) is None


def test_events_with_wrong_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'User-Agent': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_USERAGENT_SCHEMA)) is None


def test_events_without_authorization_header():
    status_code, body_json = get_response(
        'GET', URI, override_headers={'Authorization': None})
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_events_with_wrong_credentials():
    status_code, body_json = get_response(
        'GET', URI, send_wrong_credentials=True)
    assert status_code == 401
    assert validate(body_json, load_schema(WRONG_TOKEN_SCHEMA)) is None


def test_events_with_wrong_delete_method():
    status_code, body_json = get_response('DELETE', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(DELETE_NOT_ALLOWED)) is None


def test_events_with_wrong_post_method():
    status_code, body_json = get_response('POST', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(POST_NOT_ALLOWED)) is None


def test_events_with_wrong_put_method():
    status_code, body_json = get_response('PUT', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(PUT_NOT_ALLOWED)) is None


def test_events_with_wrong_patch_method():
    status_code, body_json = get_response('PATCH', URI)
    assert status_code == 405
    assert validate(body_json, load_schema(PATCH_NOT_ALLOWED)) is None


def test_events_with_right_credentials():
    status_code, body_json = get_response('GET', URI)
    assert status_code == 200
    assert validate(body_json, load_schema(EVENTS_200_SCHEMA)) is None


def test_events_with_right_credentials_and_wrong_discipline_id():
    wrong_uri = 'https://orioks.miet.ru/api/v1/student/disciplines/000/events'
    status_code, body_json = get_response('GET', wrong_uri)
    assert status_code == 400
    assert validate(body_json, load_schema(EVENTS_400_SCHEMA)) is None
