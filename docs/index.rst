.. student-api documentation master file, created by
   sphinx-quickstart on Thu Nov 29 01:34:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. toctree::
   :titlesonly:
   :hidden:

   auth
   schedule
   student/student
   micropusher/pushes
   faq


Документация студенческого API системы ОРИОКС
=============================================

На данный момент **п**\ рограммный **и**\ нтерфейс **п**\ риложения системы
`ОРИОКС`_ позволяет получить часть данных студента, доступных из веб-версии
ОРИОКС (список дисциплин, баллы по контрольным мероприятиям и т.д.), однако
стремится в будущем сделать доступным работу со всем, что доступно студенту
(включая отправку домашних заданий). В первую очередь это полезно для
разработчиков, которые хотят создать своё приложение и думают над тем, каким
образом взаимодействовать с с системой.

.. _ОРИОКС: https://orioks.miet.ru/


Руководство программиста
------------------------

.. toctree в начале документа скрыт, а не написан здесь, т.к. sphinx не
   позволяет создать ступенчатое содержание. Однако, в боковой панели, при
   открытии документа, отображаются вложенные в него.

.. Все :doc: ссылаются на сам документ. :ref: нужен для того, чтобы сослаться
   на конкретный заголовок уже упомянутого с помощью :doc: документа.

* :doc:`auth`
* :doc:`schedule`
* :doc:`student/student`
   * :doc:`student/academic_debts`
      * :doc:`student/resits`
   * :doc:`student/disciplines`
      * :doc:`student/events`
   * :doc:`student/pushes`
   * :doc:`student/tokens`
   * :ref:`token_revocation`
* :doc:`micropusher/pushes`
* :doc:`faq`
