Пуш-уведомления (в разработке)
==============================

При получении пуш-уведомления, устройство должно оповестить об этом ОРИОКС. Это
необходимо для понимания, видит ли студент уведомления.

Схема подтверждения уведомления:

.. sequenceDiagram
     ОРИОКС ->> ОРИОКС: Создание пуш-объекта в /api/v1/student/pushes
     Note over ОРИОКС: { "received": false }
     ОРИОКС ->> Firebase&APNs: Запрос отправки пуш-уведомления
     Firebase&APNs ->> Устройство: Получение уведомления
     Устройство ->> ОРИОКС: Потверждения получения уведомления
     Note over Устройство,ОРИОКС: PUT .../pushes/<push-id> { "received": true }

.. диаграмма создана в https://mermaidjs.github.io/mermaid-live-editor/
   тема цветов диаграммы: forest

.. image:: ../_charts/mermaid-diagram-20190310184715.svg

|

.. символ "трубы" добавляет пустую строку


.. _push_id:

Структура пуш-уведомления
=========================


Android
-------

.. code-block:: json

   {
     "message": {
       "token": "String",
       "notification": {
         "title": "String",
         "body": "String"
       },
       "data": {
         "orioks_push_id": "Integer"
       }
     }
   }



iOS
---

.. code-block:: json

   {
     "aps": {
       "alert": {
         "title": "String",
         "body" : "String"
       }
     },
     "data": {
       "orioks_push_id": "Integer"
     }
   }


Отправка уведомления (Внутренее)
===================================


Запрос
------

.. code-block:: http

   POST /push/send HTTP/1.1
   Accept: application/json
   Authorization: <token>
  
.. code-block:: json

   {
     "recipients: [Integer],
     "message": {
       "alert": String,
       "body": String
     }
     "data": {}
   }

Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK


Регистрация устройства пользователя
===================================


Запрос
------

.. code-block:: http

   POST /push/register HTTP/1.1
   Accept: application/json
   Authorization: <token>
  
.. code-block:: json

   {
        "userId": Int,
        "deviceUuid": String,
    	"pushKey": String,
	    "deviceType": Int
   }

Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   {
        "id": Int,
        "userId": Int,
        "deviceUuid": String,
    	"pushKey": String,
	    "deviceType": Int,
        "updatedAt": String,
        "createdAt": String
    }


Удаление устройства пользователя
===================================


Запрос
------

.. code-block:: http

   POST /push/unregister HTTP/1.1
   Accept: application/json
   Authorization: <token>
  
.. code-block:: json

   {
        "userId": Int,
        "deviceUuid": String
   }

Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK


Подтверждение получения уведомления (Не реализовано)
===================================


Запрос
------

.. code-block:: http

   PUT /api/v1/student/pushes/<push-id> HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

.. code-block:: json

   {
     "received": true
   }

``<push-id>``
   :ref:`Идентификатор <push_id>` пуш-уведомления.

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK


Возможные ошибки
----------------
.. code-block:: http

   HTTP/1.1 400 Bad Request

.. code-block:: json

   {
     "error": "Пуш-уведомления с данным идентификатором не существует"
   }