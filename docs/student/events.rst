Контрольные мероприятия дисциплины
==================================

Запрос
------

.. code-block:: http

   GET /api/v1/student/disciplines/<discipline-id>/events HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

``<discipline-id>``
   :ref:`Идентификатор <discipline_id>` дисциплины.

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.


Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "alias": "String, Optional",
       "current_grade": "Float, Optional",
       "max_grade": "Float",
       "name": "String, Optional",
       "type": "String",
       "week": "Integer"
     }
   ]

:ref:`JSONSchema <events_GET_200>` для валидации ответа.

``alias``
   Краткое название контрольного мероприятия по дисциплине.

``current_grade``
   Текущая оценка за контрольное мероприятие. Может быть трёх видов:

   * отсутствует в объекте - следует выводить "-" вместо баллов;
   * -1.0 - пропуск занятия ("н");
   * неотрицательное число - балл за к/м.

   Данный ключ отсутствует если преподаватель ещё не выставлял оценки по
   данному к/м.

``max_grade``
   Максимальная оценка за контрольное мероприятие.

``name``
   Название контрольного мероприятия. Если ключ отсутствует, следует скрывать
   поле имя дисциплины.

``type``
   Тип контрольного мероприятия.

``week``
   Неделя, на которой проводится контрольное мероприятие.


Пример
------

.. code-block:: http

   GET /api/v1/student/disciplines/39/events HTTP/1.1
   Accept: application/json
   Authorization: Bearer T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX
   User-Agent: orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "alias": "КР.1",
       "current_grade": -1.0,
       "max_grade": 5.0,
       "name": "Синтез комбинационных схем",
       "type": "Контрольная работа",
       "week": 1
     }
   ]


Возможные ошибки
----------------

.. code-block:: http

   HTTP/1.1 400 Bad Request

.. code-block:: json

   {
     "error": "Дисциплины с данным идентификатором не существует"
   }

:ref:`JSONSchema <events_GET_400_wrong_discipline_id>` для валидации ответа.

.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.
