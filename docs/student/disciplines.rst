Дисциплины текущего семестра
============================


Запрос
------

.. code-block:: http

   GET /api/v1/student/disciplines HTTP/1.1
   Accept: application/json
   Authorization: Bearer <token>
   User-Agent: <app>/<app-version> <os>[ <os-version>]

.. hint:: Информация о заполнении заголовков находится :ref:`здесь
   <filling_headers>`.

.. _discipline_id:


Ответ
-----

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "control_form": "String, Optional",
       "current_grade": "Float, Optional",
       "department": "String",
       "exam_date": "String, Optional",
       "id": "Integer",
       "max_grade": "Float",
       "name": "String",
       "teachers": [ "String, Optional" ]
     }
   ]

:ref:`JSONSchema <disciplines_GET_200>` для валидации ответа.

``control_form``
  Форма контроля. Если ключ отсутствует, выводим ``"Не назначено"``. Может быть
  одним из следующих вариантов:

.. code-block:: json

   [
     "Зачёт",
     "Дифференцированный зачёт",
     "Экзамен",
     "Курсовая работа",
     "Курсовой проект",
     "Защита ВКР"
   ]

``current_grade``
   Текущий балл по дисциплине. Если ключ отсутствует, следует выводить '-'
   (дефис).

``department``
   Кафедра, преподающая предмет.

``exam_date``
   Дата экзамена в формате ``YYYY-MM-DD`` (`ISO 8601`_). Если ключ отсутствует,
   cледует выводить "Не назначена" или вовсе скрывать поле *дата экзамена*.

.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601

``id``
   Уникальный идентификатор дисциплины.

``max_grade``
   Максимальный балл по дисциплине.

``name``
   Название дисциплины.

``teachers``
   ФИО преподавател(я/ей). Если ключ отсутствует, следует выводить "Не
   назначены" или вовсе скрывать поле преподаватели.


Пример
------

.. code-block:: http

   GET /api/v1/student/disciplines HTTP/1.1
   Accept: application/json
   Authorization: Bearer T7d7IyzKKO4ehP5Imuz0Hg1EJfcsY8QX
   User-Agent: orioks_api_test/0.1 GNU/Linux 4.17.2-1-ARCH

.. code-block:: http

   HTTP/1.1 200 OK

.. code-block:: json

   [
     {
       "control_form": "Зачёт",
       "current_grade": 50.0,
       "department": "ИГД",
       "exam_date": "2018-06-25",
       "id": 25,
       "max_grade": 55.0,
       "name": "3D моделирование",
       "teachers": [ "Соколова Татьяна Юрьевна" ]
     }
   ]

.. seealso:: В разделе :doc:`ЧаВо</faq>` указаны все коды ошибок, которые
   возвращает API.
