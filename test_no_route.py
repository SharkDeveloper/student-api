from jsonschema import validate

from api_tester import get_response, load_schema, SCHEMAS_PATH

URI = 'https://orioks.miet.ru/api/v1/wrong-uri'

WRONG_URI = SCHEMAS_PATH + '404.json'


def test_wrong_uri():
    status_code, body_json = get_response('GET', URI)
    assert status_code == 404
    assert validate(body_json, load_schema(WRONG_URI)) is None
