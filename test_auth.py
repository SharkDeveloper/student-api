# Данный тест должен запускаться самым первым, т.к. в нём происходит получение
# токена, необходимого для остальных тестов.
import os

from jsonschema import validate

from api_tester import (get_response, load_schema, SCHEMAS_PATH,
                        NO_ACCEPT_SCHEMA, NO_USERAGENT_SCHEMA,
                        WRONG_ACCEPT_SCHEMA, WRONG_USERAGENT_SCHEMA,
                        DELETE_NOT_ALLOWED, PATCH_NOT_ALLOWED,
                        POST_NOT_ALLOWED, PUT_NOT_ALLOWED)

URI = 'https://orioks.miet.ru/api/v1/auth'

AUTH_SCHEMAS_PATH = SCHEMAS_PATH + 'auth/'
AUTH_200_SCHEMA = AUTH_SCHEMAS_PATH + '200.json'
AUTH_401_SCHEMA = AUTH_SCHEMAS_PATH + '401.json'


def test_auth_without_accept_header():
    status_code, body_json = get_response(
        'GET', URI, auth_type='Basic', override_headers={'Accept': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_ACCEPT_SCHEMA)) is None


def test_auth_with_wrong_accept_header():
    status_code, body_json = get_response(
        'GET', URI, auth_type='Basic', override_headers={'Accept': 'wrong'})
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_ACCEPT_SCHEMA)) is None


def test_auth_without_useragent_header():
    status_code, body_json = get_response(
        'GET', URI, auth_type='Basic', override_headers={'User-Agent': None})
    assert status_code == 400
    assert validate(body_json, load_schema(NO_USERAGENT_SCHEMA)) is None


def test_auth_with_wrong_useragent_header():
    override_headers = {'User-Agent': 'wrong'}
    status_code, body_json = get_response(
        'GET', URI, auth_type='Basic', override_headers=override_headers)
    assert status_code == 400
    assert validate(body_json, load_schema(WRONG_USERAGENT_SCHEMA)) is None


def test_auth_without_authorization_header():
    override_headers = {'Authorization': None}
    status_code, body_json = get_response(
        'GET', URI, auth_type='Basic', override_headers=override_headers)
    assert status_code == 401
    assert validate(body_json, load_schema(AUTH_401_SCHEMA)) is None


def test_auth_with_wrong_credentials():
    status_code, body_json = get_response(
        'GET', URI, auth_type='Basic', send_wrong_credentials=True)
    assert status_code == 401
    assert validate(body_json, load_schema(AUTH_401_SCHEMA)) is None


def test_auth_with_wrong_delete_method():
    status_code, body_json = get_response('DELETE', URI, auth_type='Basic')
    assert status_code == 405
    assert validate(body_json, load_schema(DELETE_NOT_ALLOWED)) is None


def test_auth_with_wrong_post_method():
    status_code, body_json = get_response('POST', URI, auth_type='Basic')
    assert status_code == 405
    assert validate(body_json, load_schema(POST_NOT_ALLOWED)) is None


def test_auth_with_wrong_put_method():
    status_code, body_json = get_response('PUT', URI, auth_type='Basic')
    assert status_code == 405
    assert validate(body_json, load_schema(PUT_NOT_ALLOWED)) is None


def test_auth_with_wrong_patch_method():
    status_code, body_json = get_response('PATCH', URI, auth_type='Basic')
    assert status_code == 405
    assert validate(body_json, load_schema(PATCH_NOT_ALLOWED)) is None


def test_auth_with_right_credentials():
    status_code, body_json = get_response('GET', URI, auth_type='Basic')
    assert status_code == 200
    assert validate(body_json, load_schema(AUTH_200_SCHEMA)) is None
    os.environ['ORIOKS_TOKEN'] = body_json['token']


def teardown_module():
    token = os.environ.get('ORIOKS_TOKEN')
    assert token is not None
    print(' Token:', token, end='')
